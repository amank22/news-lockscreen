package com.aman.qurux.newslockscreen.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.aman.qurux.newslockscreen.R;
import com.aman.qurux.newslockscreen.customviews.AngledLinearLayout;
import com.aman.qurux.newslockscreen.customviews.TimedImageView;
import com.aman.qurux.newslockscreen.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentNewsPage.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentNewsPage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentNewsPage extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_POS = "pos";
    private static final String TAG = "FragmentNewsPager";

    private int mParamPos;

    private OnFragmentInteractionListener mListener;

    private AngledLinearLayout bottomlayout;
    private TimedImageView imageView;
    private BroadcastReceiver _broadcastReceiver;
    private final SimpleDateFormat _sdfWatchTime = new SimpleDateFormat("h:mm", Locale.ENGLISH);

    public static FragmentNewsPage newInstance(int pos) {
        FragmentNewsPage fragment = new FragmentNewsPage();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_POS, pos);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentNewsPage() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamPos = getArguments().getInt(ARG_PARAM_POS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomlayout = (AngledLinearLayout) view.findViewById(R.id.bottomlayout);
        imageView = (TimedImageView) view.findViewById(R.id.imageView_news);
        imageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), Constants.images[mParamPos]));
        Calendar c = Calendar.getInstance();
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm", Locale.ENGLISH);
        String formattedTime = sdf.format(now);
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sd = new SimpleDateFormat("MMMM dd", Locale.US);
        String formattedDate = sd.format(ca.getTime());
        imageView.setAllTimes(formattedDate, getName(c, Calendar.DAY_OF_WEEK), formattedTime, getName(c, Calendar.AM_PM));
        Palette.PaletteAsyncListener listener = new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                bottomlayout.setAllTexts(Constants.categories[mParamPos], Constants.titles[mParamPos],
                        Constants.contents[mParamPos], palette.getMutedColor(Color.MAGENTA));
            }
        };
        Palette.from(BitmapFactory.decodeResource(getResources(), Constants.images[mParamPos])).generate(listener);

    }

    private String getName(Calendar c, int field) {
        return c.getDisplayName(field, Calendar.LONG, Locale.ENGLISH);
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onSomeInteraction();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context ctx, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    Log.d(TAG, _sdfWatchTime.format(new Date()));
                    imageView.setTime(_sdfWatchTime.format(new Date()));
                }
            }
        };

        getActivity().registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (_broadcastReceiver != null)
            getActivity().unregisterReceiver(_broadcastReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSomeInteraction();
    }

}

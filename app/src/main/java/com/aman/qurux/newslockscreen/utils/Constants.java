package com.aman.qurux.newslockscreen.utils;

import com.aman.qurux.newslockscreen.R;

/**
 * Created by aman on 28-10-2015 in News LockScreen.
 */
public class Constants {

    public static final String[] categories = {"World", "Politics", "Sports", "India", "Entertainment", "Business"};

    public static final String[] titles =
            {"Refugees forced to scramble for food by police in Hungary",
                    "Sadiq Khan elected as Labour's candidate for mayor of London",
                    "Eoin Morgan smashes 92 to help England level series with Australia",
                    "India won’t fire first bullet along border: Rajnath to Pak",
                    "Fatwa against A.R. Rahman for film on Prophet",
                    "Gionee to invest $50 mn to make handsets in India"};

    public static final String[] contents =
            {"Hungarian police are investigating a video showing police in surgical masks throwing packs of sandwiches to refugees.",
                    "Sadiq Khan, the former shadow justice secretary, has won the Labour nomination for the London mayoralty.",
                    "For just the fourth time ever England chased down a target of 300 with their captain, Eoin Morgan, leading the way.",
                    "India will not fire the first bullet towards Pakistan as it wants cordial relations with all its neighbours.",
                    "Objecting to an Iranian film, a Muslim group here has issued a fatwa against music composer A.R. Rahman.",
                    "Chinese smartphone maker Gionee will invest USD 50 million over next three years to make handsets in India."};

    public static final int[] images = {R.drawable.picture1,
            R.drawable.picture2,
            R.drawable.picture3,
            R.drawable.picture4,
            R.drawable.picture5,
            R.drawable.picture6};

}

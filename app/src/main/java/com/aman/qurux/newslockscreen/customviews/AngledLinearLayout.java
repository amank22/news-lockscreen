package com.aman.qurux.newslockscreen.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * TODO: document your custom view class.
 */
public class AngledLinearLayout extends LinearLayout {

    private static final String TAG = "AngledLinearLayout";
    private final Path path = new Path();
    private final Path bpath = new Path();
    private final Paint paint = new Paint();
    private final Paint bpaint = new Paint();
    private final TextPaint catPaint = new TextPaint();
    private final TextPaint titlePaint = new TextPaint();
    private final TextPaint contentPaint = new TextPaint();

    private String title, category, content;
    private int marginCat;

    private int color = Color.CYAN;
    private boolean drawTexts = false;

    private Rect rect = new Rect();
    private Rect hitBox = new Rect();

    private StaticLayout s0, s1, s2;
    private float catYCoordinate, catXCoordinate, titleYCoordinate, titleXCoordinate, contentYCoordinate;

    public int getAngledHeight() {
        return getChildAt(3).getBottom() + getChildAt(0).getHeight();
    }

    private int angledHeight = 0;


    public AngledLinearLayout(Context context) {
        super(context);
    }

    public AngledLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AngledLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOrientation(HORIZONTAL);
        setWeightSum(getChildCount());
        category = "Category";
        title = "Title";
        content = "Content";
    }

    private static final int alpha = 255;

    private void initPaint() {
        paint.setColor(color);
        paint.setAlpha(alpha);
    }

    private void initCatPaint() {
        catPaint.setColor(color);
//        catPaint.setTextAlign(Paint.Align.RIGHT);
        catPaint.setFakeBoldText(true);
        catPaint.setTextSize(35);
        catPaint.setAlpha(alpha);
        catPaint.setDither(true);
        catPaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            catPaint.setElegantTextHeight(true);
        }
        marginCat = 20;
    }

    private void initTitlePaint() {
        titlePaint.setColor(Color.WHITE);
        titlePaint.setFakeBoldText(true);
        titlePaint.setTextSize(40);
        titlePaint.setDither(true);
        titlePaint.setAlpha(alpha);
        titlePaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            titlePaint.setElegantTextHeight(true);
        }
    }

    private void initContentPaint() {
        contentPaint.setColor(Color.WHITE);
        contentPaint.setTextSize(30);
        contentPaint.setDither(true);
        contentPaint.setAlpha(alpha);
        contentPaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            contentPaint.setElegantTextHeight(true);
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int width = canvas.getWidth();
        canvas.rotate(20, 0, 0);
        canvas.translate(30, 0);
        canvas.save();
        canvas.restore();
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setRotation(-20);
        }
        //Background Black Color
        bpath.moveTo(-30, getChildAt(0).getBottom());
        bpath.lineTo(width + 60, getChildAt(getChildCount() - 1).getBottom());
        bpath.lineTo(width + width * 2, getHeight() + 30);
        bpath.lineTo(-30, getBottom() + 30);
        bpath.lineTo(-30, getChildAt(0).getBottom());
        bpath.close();
        bpath.setFillType(Path.FillType.WINDING);
        bpaint.setColor(Color.BLACK);
        canvas.drawPath(bpath, bpaint);
        canvas.save();
        //Strip of Icons
        initPaint();
        path.moveTo(-30, 0);
        path.lineTo(width + 30, getChildAt(getChildCount() - 1).getTop());
        path.lineTo(width + 60, getChildAt(getChildCount() - 1).getBottom());
        path.lineTo(-30, getChildAt(0).getBottom());
        path.lineTo(-30, 0);
        path.close();
        path.setFillType(Path.FillType.WINDING);
        canvas.drawPath(path, paint);
        canvas.save();
        super.dispatchDraw(canvas);
        canvas.rotate(-20, 0, 0);
        canvas.save();
        canvas.restore();
        if (drawTexts) {
        /*
        * Drawing Category Text
        */
            canvas.translate(catXCoordinate, catYCoordinate);
            //draws static layout on canvas
            s0.draw(canvas);
            canvas.restore();
            canvas.rotate(-20, 0, 0);
            canvas.save();
            canvas.restore();
        /*
        * Drawing Title Text
        */
            canvas.translate(titleXCoordinate, titleYCoordinate);
            //draws static layout on canvas
            s1.draw(canvas);
            canvas.restore();
            canvas.rotate(-20, 0, 0);
            canvas.save();
            canvas.restore();
        /*
        * Drawing Content Text
        */
            canvas.translate(titleXCoordinate, contentYCoordinate);
            //draws static layout on canvas
            s2.draw(canvas);
            canvas.restore();
        }
    }


    @Override
    public void addOnLayoutChangeListener(OnLayoutChangeListener listener) {
        super.addOnLayoutChangeListener(listener);
        Log.d(TAG,"onlayoutchangelistener");
        initTextDraws();
    }

    private void initTextDraws() {
        /*
        * Drawing Category Text
        */
        initCatPaint();
        s0 = new StaticLayout(category, catPaint, getChildAt(1).getLeft() - marginCat,
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
        int numberOfCategoryLines = s0.getLineCount();
        catYCoordinate = getChildAt(0).getBottom() + getChildAt(0).getHeight() / 2 + marginCat * 2;

        //text will be drawn from left
        catXCoordinate = getTextLeft(category, catPaint);
         /*
        * Drawing Title Text
        */
        initTitlePaint();
        s1 = new StaticLayout(title, titlePaint, getChildAt(3).getLeft() - marginCat - 50,
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
        float catHeight = getTextHeight(category, catPaint);
//        int numberOfTextLines = sl.getLineCount();
        titleYCoordinate = catYCoordinate + catHeight * numberOfCategoryLines + marginCat * 2;
        titleXCoordinate = getTextLeft(category, catPaint);
        /*
        * Drawing Content Text
        */
        initContentPaint();
        s2 = new StaticLayout(content, contentPaint, getChildAt(3).getRight() - marginCat - 50,
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
        float titleHeight = getTextHeight(title, titlePaint);
        int numberOfTtitleLines = s1.getLineCount();
        contentYCoordinate = titleYCoordinate + titleHeight * numberOfTtitleLines + marginCat * 2;

    }

    /**
     * @return text height
     */
    private float getTextHeight(String text, Paint paint) {

        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.height();
    }

    /**
     * @return text left
     */
    private int getTextLeft(String text, Paint paint) {
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.left;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = getMeasuredWidth();
        int h = getMeasuredHeight();
        setMeasuredDimension(w, h);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int iconPosition = -1;
        for (int i = 0; i < getChildCount(); i++) {
            if (isTouchInView(getChildAt(i), event)) {
                iconPosition = i;
            }
        }
        if (iconPosition != -1) {
//            event.setLocation(getWidth() - event.getX(), getHeight() - event.getY());
            getChildAt(iconPosition).dispatchTouchEvent(event);
            return true;
        }
        return super.dispatchTouchEvent(event);
    }

    private boolean isTouchInView(View view, MotionEvent event) {

        view.getGlobalVisibleRect(hitBox);
        return hitBox.contains((int) event.getRawX(), (int) event.getRawY());
    }

    public void setAllTexts(String category, String title, String content, int color) {
        this.category = category;
        this.title = title;
        this.content = content;
        this.color = color;
        drawTexts = true;
        initTextDraws();
        invalidate();
    }


}

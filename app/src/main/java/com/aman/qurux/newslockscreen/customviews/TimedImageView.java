package com.aman.qurux.newslockscreen.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by aman on 28-10-2015 in News LockScreen.
 */
public class TimedImageView extends ImageView {

    private final Path path = new Path();
    private final Paint paint = new Paint();
    private final TextPaint datepaint = new TextPaint();
    private final TextPaint daypaint = new TextPaint();
    private final TextPaint timepaint = new TextPaint();
    private final TextPaint ampmpaint = new TextPaint();
    private String date;
    private String time;
    private String day;
    private String AmPm;
    private static final float margin = 10f;
    private StaticLayout s0, s1, s2, s3;

    private float dateYCoordinate, XCoordinate, dayYCoordinate, timeYCoordinate;

    public TimedImageView(Context context) {
        super(context);
    }

    public TimedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        date = "date";
        day = "day";
        time = "time";
        AmPm = "AM/PM";

    }

    public TimedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    private void DrawInital() {
        initDatePaint();
        s0 = new StaticLayout(date, datepaint, getWidth(),
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
        int numberOfDateLines = s0.getLineCount();
        float dateHeight = getTextHeight(date, datepaint);
        dateYCoordinate = 20;

        //text will be drawn from left
        XCoordinate = getTextLeft(date, datepaint);
        initDayPaint();
        s1 = new StaticLayout(day, daypaint, getWidth(),
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
        int numberOfDayLines = s1.getLineCount();
        float dayHeight = getTextHeight(day, daypaint);
        dayYCoordinate = dateYCoordinate + dateHeight * numberOfDateLines + margin;
        initTimePaint();
//        float timeHeight = getTextHeight(date, timepaint);
        timeYCoordinate = dayYCoordinate + dayHeight * numberOfDayLines + margin;
        initAmPmPaint();
//        rightboundtime = getWidth() / 200 * (200 - timeYCoordinate - timeHeight - margin);
        s2 = new StaticLayout(time, timepaint, getWidth(),
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
        s3 = new StaticLayout(getAmPm(), ampmpaint, getWidth(),
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);
    }

    private void initDatePaint() {
        datepaint.setColor(Color.WHITE);
        datepaint.setTextSize(50);
        datepaint.setDither(true);
        datepaint.setFakeBoldText(true);
        datepaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            datepaint.setElegantTextHeight(true);
        }
    }

    private void initDayPaint() {
        daypaint.setColor(Color.WHITE);
        daypaint.setTextSize(40);
        daypaint.setDither(true);
        daypaint.setFakeBoldText(false);
        daypaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            daypaint.setElegantTextHeight(true);
        }
    }

    private void initTimePaint() {
        timepaint.setColor(Color.WHITE);
        timepaint.setTextSize(60);
        timepaint.setDither(true);
        timepaint.setFakeBoldText(true);
        timepaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            timepaint.setElegantTextHeight(true);
        }
    }

    private void initAmPmPaint() {
        ampmpaint.setColor(Color.WHITE);
        ampmpaint.setTextSize(30);
        ampmpaint.setDither(true);
        ampmpaint.setFakeBoldText(true);
        ampmpaint.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ampmpaint.setElegantTextHeight(true);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        DrawInital();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        path.lineTo(getWidth(), 0);
        path.lineTo(getWidth(), getHeight() / 10);
        path.lineTo(0, getHeight() / 10 + 200);
        path.lineTo(0, 0);
        path.close();
        path.setFillType(Path.FillType.WINDING);
        paint.setDither(true);
        paint.setColor(Color.BLACK);
        canvas.drawPath(path, paint);
        canvas.save();
        /*
        * Drawing Date Text
        */
        canvas.translate(XCoordinate, dateYCoordinate);
        //draws static layout on canvas
        s0.draw(canvas);
        canvas.restore();
        /*
        * Drawing Day Text
        */
        canvas.save();
        //text will be drawn from left
        canvas.translate(XCoordinate, dayYCoordinate);
        //draws static layout on canvas
        s1.draw(canvas);
        canvas.restore();
        /*
        * Drawing Time Text
        */
        canvas.save();
        //text will be drawn from left
        canvas.translate(XCoordinate, timeYCoordinate);
        //draws static layout on canvas
        s2.draw(canvas);
        canvas.restore();
        /*
        * Drawing AM/PM Text
        */
        canvas.save();
        //text will be drawn from left
        canvas.translate(getTextWidth(time, timepaint) + margin, timeYCoordinate + getTextHeight(getAmPm(), ampmpaint) / 2);
        //draws static layout on canvas
        s3.draw(canvas);
        canvas.restore();

    }
    Rect rect = new Rect();
    /**
     * @return text height
     */
    private float getTextHeight(String text, Paint paint) {

        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.height();
    }

    /**
     * @return text left
     */
    private int getTextLeft(String text, Paint paint) {
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.left;
    }

    /**
     * @return text width
     */
    private int getTextWidth(String text, Paint paint) {
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.width();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        DrawInital();
        invalidate();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
        DrawInital();
        invalidate();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
        DrawInital();
        invalidate();
    }

    public String getAmPm() {
        return AmPm;
    }

    public void setAmPm(String amPm) {
        AmPm = amPm;
        DrawInital();
        invalidate();
    }

    public void setAllTimes(String date, String day, String time, String amPm) {
        this.date = String.valueOf(date);
        this.day = String.valueOf(day);
        this.time = String.valueOf(time);
        AmPm = String.valueOf(amPm);
        DrawInital();
        invalidate();
    }
}

package com.aman.qurux.newslockscreen.customviews;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;

/**
 * Created by aman on 26-10-2015 in News LockScreen.
 */
public class SwipeUpViewPager extends ViewPager {

    public interface OnScreenUnlocked {
        void onUnlocked();
    }

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 0;
    private static final String TAG = "SwipeUpViewPager";
    private static boolean isUnlocking = false;

    private final int loc[] = new int[2];

    private GestureDetectorCompat mDetector;
    private OnScreenUnlocked mlistener;

    private int frameHeight;

    public SwipeUpViewPager(Context context) {
        super(context);
        init();
    }

    public SwipeUpViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mDetector = new GestureDetectorCompat(getContext(), new MyGestureListener());
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isUnlocking;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_UP:
                getLocationInWindow(loc);
                frameHeight = getHeight();
                if (Math.abs(loc[1]) <= frameHeight / 3) {
                    ObjectAnimator anim = ObjectAnimator.ofFloat(this, "Y", 0f);
                    anim.setInterpolator(new BounceInterpolator());
                    anim.setDuration(600);
                    anim.start();
                } else {
                    AnimateEnd();
                }
                break;

        }
        mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private void AnimateEnd() {
        ObjectAnimator anim = ObjectAnimator.ofFloat(this, "Y", -frameHeight);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(300);
        anim.start();
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                Log.d(TAG, "Animation end");
                if (mlistener != null) {
                    mlistener.onUnlocked();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";
        private float startY;

        @Override
        public boolean onDown(MotionEvent event) {
//            Log.d(DEBUG_TAG, "onDown: " + event.getY());
            startY = getY();
            isUnlocking = false;
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
//            Log.d(DEBUG_TAG, "onScroll: " + (startY - e2.getY()));
            if (distanceY > distanceX) {
                isUnlocking = true;
                if (e2.getY() <= e1.getY()) {
//                Log.i(TAG, "Scrolling towards top");
                    startY -= Math.abs(distanceY) / 2;
                }
                setY(startY);
                invalidate();
            } else {
                isUnlocking = false;
            }
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
//            Log.d(DEBUG_TAG, "onFling: " + velocityY);

            try {
                // downward swipe
                if (e1.getY() - e2.getY() < SWIPE_MAX_OFF_PATH) {
                    isUnlocking = false;
//                    Log.d(DEBUG_TAG, "onFling Downward: " + (e1.getY() - e2.getY()));
                } else if (e2.getY() - e1.getY() < SWIPE_MAX_OFF_PATH) {
                    isUnlocking = true;
//                    Log.d(DEBUG_TAG, "onFling Upward: " + velocityY);
                    if (velocityY < -1000) {
                        AnimateEnd();
                    }
                }
                // right to left swipe
                else if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE) {
                    isUnlocking = false;
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE) {
                    isUnlocking = false;
                }
            } catch (Exception e) {
                // nothing
            }
            return true;
        }
    }

    public void setOnUnlockListener(OnScreenUnlocked onl) {
        mlistener = onl;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mlistener != null)
            mlistener = null;
    }


}
